import React from "react";
import { Switch, Route } from "react-router-dom";
import { CalendarDiary } from "../components/calendar/CalendarDiary";
import { GalleryScreen } from "../components/galery/GalleryScreen";
import { SideBar } from "../components/ui/Drawer";

export const DashboardRoutes = () => {
  return (
    <div className="bg-white">
      <SideBar />

      <div className="container mt-2 ">
        <Switch>
          <Route exact path="/" component={CalendarDiary} />        
        
        </Switch>
      </div>
    </div>
  );
};
