import { makeStyles } from "@material-ui/core";

const drawerWidth = 240;
export const uiStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  appBar: {
    background:"#292929",
    color: "#e7d3af",
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
},
appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
    }),
},
menuButton: {
  marginRight: theme.spacing(2),
    //marginRight: 36,
 
},
title: {
  flexGrow: 1,
},
hide: {
    display: "none",
   
},
logout:{
  textAlign: "center",
 // marginLeft: "100px"
},
drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
},
drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
    }),
    backgroundColor: "#292929",
    color: "#fafbfd"
  },
drawerClose: {
    transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9) + 1,
    },
    backgroundColor: "#292929",
   
},
toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));
