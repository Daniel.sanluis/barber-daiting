import React from "react";
import { useSelector, useDispatch } from "react-redux";
import clsx from "clsx";
import { useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";

import MailIcon from "@material-ui/icons/Mail";
import { uiStyles } from "../../styles/ui.Style";
import { startLogout } from "../../actions/auth";
import { Button } from "@material-ui/core";


export function SideBar() {

  const dispatch = useDispatch();
  const classes = uiStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const { name } = useSelector((state) => state.auth);


  const handleDrawerOpen = () => {
   // setOpen(true);
  };

  const handleDrawerClose = () => {
   // setOpen(false);
  };

  const handleLogout = () => {
    dispatch(startLogout());
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <Typography className={classes.title} variant="h6" noWrap>
            Barber Daiting App
          </Typography>
          <small>{name}</small>
          <Button className={classes.logout} onClick={handleLogout} color="inherit">Logout</Button>
     
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        <List>
            <ListItem button >
              <ListItemIcon style={{ color: "#e7d3af" }}>
                 <MailIcon />
              </ListItemIcon>
              <ListItemText primary={"Agenda"} />
            </ListItem>
            <ListItem button  >
              <ListItemIcon style={{ color: "#e7d3af" }}>
                 <MailIcon />
              </ListItemIcon>
              <ListItemText primary={"Galeria"} />
            </ListItem>
            <ListItem button >
              <ListItemIcon style={{ color: "#e7d3af" }}>
                 <MailIcon />
              </ListItemIcon>
              <ListItemText primary={"Contacto"} />
            </ListItem>
          
        </List>
        <Divider />
      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbar} />
      </main>
    </div>
  );
}
