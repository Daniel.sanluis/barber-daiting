import React from 'react';
import ReactDOM from 'react-dom';
import { BarberDaitingApp } from './App';


import './styles.css';

ReactDOM.render(
    <BarberDaitingApp />,
  document.getElementById('root')
);

